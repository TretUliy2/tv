# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class ReceiverType(models.Model):
    name = models.CharField(max_length=120)
    input_count = models.IntegerField()
    INPUT_TYPE_CHOICES = (
        (0, 'S'),
        (1, 'S2'),
        (2, 'T'),
        (3, 'T2'),
        (4, 'C'),
        (5, 'C2'),
        (6, 'S/S2'),
        (7, 'S/S2/T/T2/C'),
    )
    input_type = models.IntegerField(choices=INPUT_TYPE_CHOICES)
    has_ci = models.BooleanField()
    ci_count = models.IntegerField()

    def __str__(self):
        return str.format("%s" % (self.name))


class MulticastOutput(models.Model):
    ip = models.GenericIPAddressField()
    receiver = models.ForeignKey('Receiver', null=True, blank=True)

    def __str__(self):
        return str.format("%s" % (self.ip))


class Receiver(models.Model):
    serial_number = models.CharField(max_length=100)
    model = models.ForeignKey('ReceiverType', on_delete=models.DO_NOTHING)
    management_ip = models.GenericIPAddressField(null=False, default="10.71.20.11")
    headend = models.ForeignKey('Headend', null=True)

    def __str__(self):
        return str.format("%s IP : %s" % (self.model, self.management_ip))


class ReceiverInput(models.Model):
    receiver = models.ForeignKey('Receiver', on_delete=models.CASCADE)
    input_number = models.IntegerField()
    transponder = models.ForeignKey('SatTransponder', on_delete=models.DO_NOTHING, null=True, blank=True)

    def __str__(self):
        return str.format("%s INPUT-%s" % (self.receiver, self.input_number))

    class Meta:
        unique_together = ('receiver', 'input_number')


class CiSlots(models.Model):
    receiver = models.ForeignKey('Receiver')
    cam_number = models.PositiveSmallIntegerField(null=False)
    cam_module = models.ForeignKey('CamModule', null=True, blank=True)

    def __str__(self):
        return str.format("%s %s" % (self.receiver, self.cam_number))


class Satelite(models.Model):
    orbital_position = models.CharField(max_length=12)
    name = models.CharField(max_length=25)

    def __str__(self):
        return str.format("%s %s" % (self.name, self.orbital_position))


class Channel(models.Model):
    title = models.CharField(max_length=50)
    right_owner = models.CharField(max_length=100)


class Headend(models.Model):
    address = models.CharField(max_length=100)

    def __str__(self):
        return str.format("%s" % (self.address, ))


class Transponder(models.Model):
    frequency = models.IntegerField()
    frequency_units = models.CharField(max_length=12, default="Mhz")
    symbol_rate = models.IntegerField()
    symbol_rate_units = models.CharField(default="Ksym/s", max_length=25)

    def __str__(self):
        return "%s _ %s" % (self.frequency, self.symbol_rate)


class SatTransponder(Transponder):
    MODULATION_TYPE_CHOICES = (
        (0, 'QPSK'),
        (1, '8PSK'),
        (2, '16APSK'),
        (3, '32APSK'),
    )
    POLARIZATION_TYPE_CHOICES = (
        (0, "H"),
        (1, "V"),
    )
    DVB_TYPE_CHOICES = (
        (0, "S"),
        (1, "S2"),
    )

    sat = models.ForeignKey('Satelite', null=False)
    polarization = models.IntegerField(default=0, choices=POLARIZATION_TYPE_CHOICES)
    modulation = models.IntegerField(choices=MODULATION_TYPE_CHOICES)
    transponder_type = models.IntegerField(choices=DVB_TYPE_CHOICES)

    def __str__(self):
        return str.format(
            "%s %s %s %s" % (self.sat.name, self.frequency, self.get_polarization_display(), self.frequency))


class CableTransponder(Transponder):
    MODULATION_TYPE_CHOICES = (
        (0, 'QAM64'),
        (1, 'QAM128'),
        (2, 'QAM256'),
    )
    modulation = models.IntegerField(choices=MODULATION_TYPE_CHOICES)

    def __str__(self):
        return str.format("%s %s %s" % (self.frequency, self.modulation, self.symbol_rate))


class Source(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class CamModule(models.Model):
    CAS_TYPE_CHOICES = (
        (0, "Viaccess"),
        (1, "Irdetto"),
        (2, "Conax"),
        (3, "NDS VideoGuard"),
        (4, "Verimatrix"),
    )
    serial = models.CharField(max_length=60)
    cas = models.IntegerField(choices=CAS_TYPE_CHOICES)
    card = models.ForeignKey('SmartCard', null=True)

    def __str__(self):
        return str.format("%s  %s" % (self.get_cas_display(), self.serial))


class Destination(models.Model):
    destination_type = models.CharField(max_length=25)


class CableDestination(Destination):
    cable_transponder = CableTransponder


class Transformer(models.Model):
    title = models.CharField(max_length=50)


class Transcoder(models.Model):
    title = models.CharField(max_length=50)
    TRANSCODING_TYPES = (
        (0, "h263toh264"),
        (1, "h264toh263"),
    )
    transcoding_type = models.IntegerField(choices=TRANSCODING_TYPES)


class SmartCard(models.Model):
    card_id = models.CharField(max_length=25)

    def __str__(self):
        return str.format("%s" % (self.card_id, ))


class Descrambler(models.Model):
    name = models.CharField(max_length=50)
    modules_count = models.IntegerField()
