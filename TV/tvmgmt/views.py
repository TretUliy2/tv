# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import *
from rest_framework import serializers, viewsets


class ReceiversSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Receiver
        fields = ('model', )


class ReceiverViewSet(viewsets.ModelViewSet):
    queryset = Receiver.objects.all()
    serializer_class = ReceiversSerializer


def index(request):
    b = Receiver.objects.all()
    return render(request, "index.html", { 'receivers' : b, })


# Create your views here.

