# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
#
#  Register your models here.


class ReceiversCamInline(admin.StackedInline):
    model = CiSlots
    extra = 0
    classes = ['collapse']


class ReceiverOutputsInline(admin.StackedInline):
    model = MulticastOutput
    extra = 0
    classes = ['collapse']


class ReceiverInputsInline(admin.StackedInline):
    model = ReceiverInput
    extra = 0
    classes = ['collapse']


class ReceiverAdmin(admin.ModelAdmin):
    inlines = [
        ReceiverInputsInline,
        ReceiverOutputsInline,
        ReceiversCamInline,
    ]


admin.site.register(Receiver, ReceiverAdmin)
admin.site.register(Headend)
admin.site.register(CamModule)
admin.site.register(SatTransponder)
admin.site.register(CableTransponder)
admin.site.register(Satelite)
admin.site.register(ReceiverInput)
admin.site.register(ReceiverType)
admin.site.register(MulticastOutput)


