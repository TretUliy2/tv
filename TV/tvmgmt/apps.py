# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig

class TvmgmtConfig(AppConfig):
    name = 'tvmgmt'

    def ready(self):
        print("Ready called")
        from . import signals