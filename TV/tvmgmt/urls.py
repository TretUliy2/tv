from django.conf.urls import url, include

from . import views
from .views import ReceiverViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'receivers', ReceiverViewSet)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/v1/', include(router.urls)),
]