from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from .models import *


@receiver(pre_save, sender=Receiver)
def receiver_pre_save(sender, **kwargs):
    print("Pre save receiver was called")
    receiver = kwargs['instance']
    receiver_id = receiver.id
    receiver_old = Receiver.objects.filter(pk=receiver_id)
    if receiver.model != receiver_old.model:
        print("New and old objects are not equal number_of_inputs was %s and now %s" % (receiver.model, receiver_old.model))
    else:
        print("New and old objects are equal")
    # if not kwargs['created']:
    #     print("Receiver pre_save modification")
    # else:
    #     print("Receiver pre_save creation")


@receiver(post_save, sender=Receiver)
def receiver_post_change(sender, **kwargs):
    print("Signal received that receiver object was changed")
    if kwargs['created']:
        receiver_local = kwargs['instance']
        for i in range(1, receiver_local.model.input_count + 1):
            print("saving transponder %d %s" % (i, receiver_local))
            e = ReceiverInput(receiver=receiver_local, input_number=i, transponder=None)
            e.save()

    else:
        print("Not a new receiver!!!")
